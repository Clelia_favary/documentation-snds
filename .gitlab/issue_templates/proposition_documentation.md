<!--
Merci pour votre proposition. Elles sont très utiles pour améliorer la documentation collaborative. 

S'il s'agit d'une question, vous n'aurez pas de réponse ici mais sur le forum d'entraide à l'adresse https://entraide.health-data-hub.fr/c/faq


↑↑ Vous pouvez préciser le titre ci-dessous ↑↑

↓↓ Le reste est à écrire ci-dessous ↓↓

Donnez un maximum de contexte et de détails.
-->















/label ~"Rédaction"